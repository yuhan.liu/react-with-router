import React from 'react';
import {Link} from 'react-router-dom';

const AboutUs = () => {
    return (
        <div>
            Company: ThoughtWorks local<br />
            Location: Xi'an<br /><br />
            For more information, please view our <Link to='/'>website</Link>.
        </div>
    );
};
export default AboutUs;
