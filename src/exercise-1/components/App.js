import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, NavLink} from 'react-router-dom';
import Home from "./Home";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import {Redirect, Route} from 'react-router';
import Products from "./Products";
import ProductDetail from "./ProductDetail";

class App extends Component {
    render() {
        return (
            <div className="app">
                <Router>
                    <div>
                        <header>
                            <ul className='header'>
                                <li>
                                    <NavLink exact to="/" activeStyle={{
                                        textDecoration: 'underline'
                                    }}>Home</NavLink>
                                </li>
                                <li>
                                    <NavLink exact to="/products" activeStyle={{
                                        textDecoration: 'underline'
                                    }}>Products</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/my-profile" activeStyle={{
                                        textDecoration: 'underline'
                                    }}>My Profile</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/about-us" activeStyle={{
                                        textDecoration: 'underline'
                                    }}>About Us</NavLink>
                                </li>
                            </ul>
                        </header>
                        <hr/>
                        <switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/products" component={Products}/>
                            <Route path='/products/:id' component={ProductDetail}></Route>
                            <Route path="/my-profile" component={MyProfile}/>
                            <Route path="/about-us" component={AboutUs}/>
                            <Route path="/goods" render={() => (
                                <Redirect to="/products"/>)}/>
                        </switch>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
