import React from 'react';
import data from '../../exercise-2/mockups/data';
import {Link} from 'react-router-dom';

class Products extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        this.setState({
            products: Object.values(data)
        });
    }

    render() {
        const {products} = this.state;
        return (
            <div>
                <ul>
                    {products.map(item => (
                        <li key={item.id}>
                            <Link to={`/products/${item.id}`}>{item.name}</Link></li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default Products;